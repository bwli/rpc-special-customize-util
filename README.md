# special-customiz-util 

| version |    date    | author | description |
| :-----: | :--------: | :----: | :---------: |
|   1.0   | 2021-09-08 |  bwli  |     new     |

| version |    date    | author | description |
| :-----: | :--------: | :----: | :---------: |
|   2.0   | 2021-09-18 |  bwli  |   add 2.1   |



#### token:

```
eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJ0aW1hdmVuIiwiZGlzcGxheU5hbWUiOiJTdXBlciBBZG1pbiIsInNjb3BlIjpbInVzZXJfaW5mbyIsInJlYWQiLCJ3cml0ZSJdLCJuYW1lIjoidGltYXZlbiIsImlwQWRkcmVzcyI6bnVsbCwidGVuYW50SWQiOiJ0ZW5hbnQxIiwiaWQiOjEsImV4cCI6MjI2MjI0NDI5OSwiYXV0aG9yaXRpZXMiOlsiUk9MRV9zdXBlcl9hZG1pbiJdLCJqdGkiOiJwdmVkQm9QYWMzaHd6WUF1NjFPVFB5QXdUQmsiLCJ0ZW5hbnQiOiJ0ZW5hbnQxIiwiY2xpZW50X2lkIjoidGltYXZlbi1jbGllbnQifQ.ZPjaxYjffVgICRZpzFSQwiH5lzq50C92hYpYnhnx8P9sT6RNpVfbPg4FLtBhAlP_Ru49dE8_XTjoc8DibwheMGKM9oDVAuMnLGHnHNxjivpWU9lK7xTsnfwi_EvjLbTBYb2T-y7M90U2HcPF8g3P7701_f4gJl-_9X1Lz5r-EGMnL36ZeJ6qUZ7w2CU59X9ZDn2CQfDfyNiFAksXdsgSHVLsMCQrPmr3X4wwTjWPbjf28ogBOGzR2oE1o64oLJUjy06QxGicOcW_ED_-2xnXAeXImM0dg2wdZGdYoqnHj-CRhBd3EF3oYAKqCadCV6gw8E1ggm_aZNgSK0HFjlpCtA
```



### 1、主要工作

- 设置不同操作环境（prod、dev、test）

- 集成mybatis plus对postgresql数据库进行操作

- 集成MongoDB查询相关cost code数据
- 配置mybatis代码生成器生成 entity、service、impl、mapper、mapper.xml
- 编写controller三个根据不同参数获取账单信息的api
- 编写获取利润不同的计算方式
- 组装api具体返回



### 2、API

##### **API 1:** 

- url： http://localhost:8091/profit/v1/employee-profit-details 
- description：获取指定员工在指定项目中的某一段时间内的所有成本利润信息

- 请求实例：

```json
input：
{
  "employeeIds": [
    "MCADG","GASPJ"
  ],
  "endDate": "2021-09-01",
  "projectId": 500,
  "startDate": "2021-07-01"
}
```

```json
output:
{
    "MCADG": [
        {
            "costCode": "10-T100-100-01",
            "cost": 1104.4000000,
            "billed": 0,
            "profit": -1104.4000000,
            "totalHour": 16.08
        },
        {
            "costCode": "10-T100-100-02",
            "cost": 717.2000000,
            "billed": 717.2000000,
            "profit": 0,
            "totalHour": 9.04
        },
        {
            "costCode": "10-T100-100-08",
            "cost": 1100.00000,
            "billed": 3200.0,
            "profit": 2100.00000,
            "totalHour": 16
        }
    ],
    "GASPJ": [
        {
            "costCode": "10-T100-100-01",
            "cost": 578.8800000,
            "billed": 0,
            "profit": -578.8800000,
            "totalHour": 12.08
        },
        {
            "costCode": "10-T100-100-02",
            "cost": 577.4400000,
            "billed": 577.4400000,
            "profit": 0,
            "totalHour": 11.04
        },
        {
            "costCode": "10-T100-100-05",
            "cost": 936.00000,
            "billed": 1136.00000,
            "profit": 200.00000,
            "totalHour": 20
        }
    ]
}
```

##### **API 2:** 

- url：  http://localhost:8091/profit/v1/cost-code-profit-details 
- description：获取指定cost code在指定项目中的某一段时间内的所有成本利润信息

- 请求实例：

```json
input:
{
  "costCodes":[
      "10-T100-100-03",
      "10-T100-100-04"
  ],
  "endDate": "2021-09-01",
  "projectId": 500,
  "startDate": "2021-07-01"
}
```

```json
output:
{
    "10-T100-100-04": [
        {
            "cost": 157.50000,
            "billed": 157.65750000,
            "profit": 0.15750000,
            "totalHour": 6
        }
    ],
    "10-T100-100-03": [
        {
            "cost": 360.00000,
            "billed": 400.0000,
            "profit": 40.00000,
            "totalHour": 8
        }
    ]
}
```

##### **API 3:** 

- url：  http://localhost:8091/profit/v1/all-profit-details 
- description：获取在指定项目中的某一段时间内的所有成本利润信息

- 请求实例：

```json
input:
{
  "endDate": "2021-09-01",
  "projectId": 500,
  "startDate": "2021-07-01"
}
```

```json
output:
[
    {
        "costCode": "10-T100-100-01",
        "costCodeCost": 2894.3284000,
        "costCodeBilled": 0,
        "costCodeProfit": -2894.3284000,
        "employeeVOS": [
            {
                "employeeID": "BARRD",
                "firstName": "DERRIN",
                "lastName": "BARRAS",
                "cost": 68.4000000,
                "billed": 0,
                "profit": -68.4000000,
                "totalHour": 1.52
            },
            {
                "employeeID": "BECBR",
                "firstName": "BRENNON",
                "lastName": "BECNEL",
                "cost": 135.4500000,
                "billed": 0,
                "profit": -135.4500000,
                "totalHour": 4.52
            },
            {
                "employeeID": "GASPJ",
                "firstName": "JONATHAN",
                "lastName": "GASPARD",
                "cost": 578.8800000,
                "billed": 0,
                "profit": -578.8800000,
                "totalHour": 12.08
            },
            {
                "employeeID": "GUIDE",
                "firstName": "EDWIN",
                "lastName": "GUIDRY III",
                "cost": 312.5584000,
                "billed": 0,
                "profit": -312.5584000,
                "totalHour": 4.52
            },
            {
                "employeeID": "LAMBC",
                "firstName": "CASEY",
                "lastName": "LAMBERT",
                "cost": 326.0000000,
                "billed": 0,
                "profit": -326.0000000,
                "totalHour": 4.52
            },
            {
                "employeeID": "MCADG",
                "firstName": "GREG",
                "lastName": "MCADAMS",
                "cost": 1104.4000000,
                "billed": 0,
                "profit": -1104.4000000,
                "totalHour": 16.08
            },
            {
                "employeeID": "VERDC",
                "firstName": "CHRISTOPHER",
                "lastName": "VERDIN",
                "cost": 368.6400000,
                "billed": 0,
                "profit": -368.6400000,
                "totalHour": 7.52
            }
        ]
    },
    {
        "costCode": "10-T100-100-02",
        "costCodeCost": 2157.5500000,
        "costCodeBilled": 2157.5500000,
        "costCodeProfit": 0,
        "employeeVOS": [
            {
                "employeeID": "BARRD",
                "firstName": "DERRIN",
                "lastName": "BARRAS",
                "cost": 0.00000,
                "billed": 0.00000,
                "profit": 0,
                "totalHour": 0
            },
            {
                "employeeID": "BECBR",
                "firstName": "BRENNON",
                "lastName": "BECNEL",
                "cost": 67.9500000,
                "billed": 67.9500000,
                "profit": 0,
                "totalHour": 2.52
            },
            {
                "employeeID": "GASPJ",
                "firstName": "JONATHAN",
                "lastName": "GASPARD",
                "cost": 577.4400000,
                "billed": 577.4400000,
                "profit": 0,
                "totalHour": 11.04
            },
            {
                "employeeID": "GUIDE",
                "firstName": "EDWIN",
                "lastName": "GUIDRY III",
                "cost": 441.32000,
                "billed": 441.32000,
                "profit": 0,
                "totalHour": 7
            },
            {
                "employeeID": "LAMBC",
                "firstName": "CASEY",
                "lastName": "LAMBERT",
                "cost": 145.000000,
                "billed": 145.000000,
                "profit": 0,
                "totalHour": 2.4
            },
            {
                "employeeID": "MCADG",
                "firstName": "GREG",
                "lastName": "MCADAMS",
                "cost": 717.2000000,
                "billed": 717.2000000,
                "profit": 0,
                "totalHour": 9.04
            },
            {
                "employeeID": "VERDC",
                "firstName": "CHRISTOPHER",
                "lastName": "VERDIN",
                "cost": 208.6400000,
                "billed": 208.6400000,
                "profit": 0,
                "totalHour": 4.52
            }
        ]
    },
    {
        "costCode": "10-T100-100-03",
        "costCodeCost": 360.00000,
        "costCodeBilled": 400.0000,
        "costCodeProfit": 40.00000,
        "employeeVOS": [
            {
                "employeeID": "BARRD",
                "firstName": "DERRIN",
                "lastName": "BARRAS",
                "cost": 360.00000,
                "billed": 400.0000,
                "profit": 40.00000,
                "totalHour": 8
            }
        ]
    },
    {
        "costCode": "10-T100-100-04",
        "costCodeCost": 157.50000,
        "costCodeBilled": 157.65750000,
        "costCodeProfit": 0.15750000,
        "employeeVOS": [
            {
                "employeeID": "BECBR",
                "firstName": "BRENNON",
                "lastName": "BECNEL",
                "cost": 157.50000,
                "billed": 157.65750000,
                "profit": 0.15750000,
                "totalHour": 6
            }
        ]
    },
    {
        "costCode": "10-T100-100-05",
        "costCodeCost": 936.00000,
        "costCodeBilled": 1136.00000,
        "costCodeProfit": 200.00000,
        "employeeVOS": [
            {
                "employeeID": "GASPJ",
                "firstName": "JONATHAN",
                "lastName": "GASPARD",
                "cost": 936.00000,
                "billed": 1136.00000,
                "profit": 200.00000,
                "totalHour": 20
            }
        ]
    },
    {
        "costCode": "10-T100-100-06",
        "costCodeCost": 259.60000,
        "costCodeBilled": 260.60000,
        "costCodeProfit": 1.00000,
        "employeeVOS": [
            {
                "employeeID": "GUIDE",
                "firstName": "EDWIN",
                "lastName": "GUIDRY III",
                "cost": 259.60000,
                "billed": 260.60000,
                "profit": 1.00000,
                "totalHour": 4
            }
        ]
    },
    {
        "costCode": "10-T100-100-07",
        "costCodeCost": 400.00000,
        "costCodeBilled": 500.0,
        "costCodeProfit": 100.00000,
        "employeeVOS": [
            {
                "employeeID": "LAMBC",
                "firstName": "CASEY",
                "lastName": "LAMBERT",
                "cost": 400.00000,
                "billed": 500.0,
                "profit": 100.00000,
                "totalHour": 7
            }
        ]
    },
    {
        "costCode": "10-T100-100-08",
        "costCodeCost": 1100.00000,
        "costCodeBilled": 3200.0,
        "costCodeProfit": 2100.00000,
        "employeeVOS": [
            {
                "employeeID": "MCADG",
                "firstName": "GREG",
                "lastName": "MCADAMS",
                "cost": 1100.00000,
                "billed": 3200.0,
                "profit": 2100.00000,
                "totalHour": 16
            }
        ]
    },
    {
        "costCode": "10-T100-100-09",
        "costCodeCost": 368.00000,
        "costCodeBilled": 0,
        "costCodeProfit": -368.00000,
        "employeeVOS": [
            {
                "employeeID": "VERDC",
                "firstName": "CHRISTOPHER",
                "lastName": "VERDIN",
                "cost": 368.00000,
                "billed": 0,
                "profit": -368.00000,
                "totalHour": 8
            }
        ]
    }
]
```

##### param:

|  attribute  | description |    type     | required |
| :---------: | :---------: | :---------: | :------: |
|  projectId  |   项目id    |    Long     |   true   |
| employeeIds |  员工编号   | Set<String> |   true   |
|  costCodes  | 成本代码id  | Set<String> |   true   |
|  startDate  |  开始时间   |  LocalDate  |   true   |
|   endDate   |  结束时间   |  LocalDate  |   true   |

##### 2.1、employee的一周的工时必须在40小时以内，否则billing计算可能会出错

- 如果cost_code以02结尾，cost = base_ot  * st_hour
- 如果cost_code以03结尾，cost = base_dt  * st_hour
- 其他 cost = base_st  * st_hour

### 3、计算公式

```java
Type1: 	billing = 0.00
Type2:	billing = cost 
Type3:	billing = cost / (1-percent margin)
Type4: 	billing = cost + (cost * percent of cost)
Type5:	billing = cost + (number of units * profit per unit)
Type6:	billing = cost + profit 
Type7:	billing = profit
Type8:	billing = profit * number of units
Type9:	TBD
利润百分比 1>percentMargin<=0
成本百分比 percentOfCost/100
总时间    number of units
```



### 4、操作数据库

- ##### postgresql


```yaml
url: jdbc:postgresql://3.140.246.69:5432/timekeeping # second computer
username: timekeeping
password: escORown
platform: postgresql
```

- ##### mongodb


```yaml
uri: mongodb://xkong:l00nie@18.223.255.167:27017/RPC_xkong_test
```



### 5、项目文档

- swagger地址：http://ec2-3-22-188-185.us-east-2.compute.amazonaws.com:8091/profit/swagger-ui.html



### 6、开发环境

| 工具 | 版本号 | 下载                                                         |
| ---- | ------ | ------------------------------------------------------------ |
| JDK  | 15     | https://download.java.net/openjdk/jdk15/ri/openjdk-15+36_windows-x64_bin.zip |

