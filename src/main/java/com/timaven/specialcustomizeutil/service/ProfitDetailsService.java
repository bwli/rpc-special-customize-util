package com.timaven.specialcustomizeutil.service;


import com.timaven.specialcustomizeutil.entity.dto.AllProfitDetailsDTO;
import com.timaven.specialcustomizeutil.entity.dto.CostCodeProfitDetailsDTO;
import com.timaven.specialcustomizeutil.entity.dto.EmployeeProfitDetailsDTO;
import com.timaven.specialcustomizeutil.entity.vo.AllProfitDetailsVO;
import com.timaven.specialcustomizeutil.entity.vo.CostCodeProfitDetailsVO;
import com.timaven.specialcustomizeutil.entity.vo.EmployeeProfitDetailsVO;
import java.util.ArrayList;
import java.util.Map;

public interface ProfitDetailsService  {
    /**
     * getEmployeeProfitDetails
     * @param employeeProfitDetailsDTO
     * @return
     */
    Map<String, ArrayList<EmployeeProfitDetailsVO>> getEmployeeProfitDetails(EmployeeProfitDetailsDTO employeeProfitDetailsDTO);

    /**
     * getCostCodeProfitDetails
     * @param costCodeProfitDetailsDTO
     * @return
     */
    Map<String, ArrayList<CostCodeProfitDetailsVO>> getCostCodeProfitDetails(CostCodeProfitDetailsDTO costCodeProfitDetailsDTO);

    /**
     * getAllProfitDetails
     * @param allProfitDetailsDTO
     * @return
     */
    ArrayList<AllProfitDetailsVO> getAllProfitDetails(AllProfitDetailsDTO allProfitDetailsDTO);
}
