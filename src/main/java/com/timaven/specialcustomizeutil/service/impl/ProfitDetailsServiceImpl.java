package com.timaven.specialcustomizeutil.service.impl;

import com.timaven.specialcustomizeutil.entity.dto.AllProfitDetailsDTO;
import com.timaven.specialcustomizeutil.entity.dto.CostCodeProfitDetailsDTO;
import com.timaven.specialcustomizeutil.entity.dto.EmployeeProfitDetailsDTO;
import com.timaven.specialcustomizeutil.entity.queryResult.ProfitDetailsQueryResult;
import com.timaven.specialcustomizeutil.entity.vo.AllProfitDetailsVO;
import com.timaven.specialcustomizeutil.entity.vo.CostCodeProfitDetailsVO;
import com.timaven.specialcustomizeutil.entity.vo.EmployeeProfitDetailsVO;
import com.timaven.specialcustomizeutil.entity.vo.EmployeeVO;
import com.timaven.specialcustomizeutil.mapper.AllocationTimeMapper;
import com.timaven.specialcustomizeutil.service.ProfitDetailsService;
import com.timaven.specialcustomizeutil.utils.ProfitTypeFormulaUtil;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

@Service
public class ProfitDetailsServiceImpl implements ProfitDetailsService {
    @Resource
    private AllocationTimeMapper allocationTimeMapper;
    @Resource
    private MongoTemplate mongoTemplate;

    @Override
    public Map<String, ArrayList<EmployeeProfitDetailsVO>> getEmployeeProfitDetails(EmployeeProfitDetailsDTO dto) {
        Map<String, ArrayList<EmployeeProfitDetailsVO>> map = new HashMap<>(16);
        Set<String> employeeIds = dto.getEmployeeIds();
        // Get the corresponding st_cost, dt_cost, ot_cost data for each employee
        for (String employeeId : employeeIds) {
            ArrayList<ProfitDetailsQueryResult> profitDetailsQueryResults = allocationTimeMapper.getEmployeeProfitDetails(dto.getProjectId(),
                    dto.getStartDate(),
                    dto.getEndDate(),
                    employeeId);
            // Remove the empty elements in the list
            profitDetailsQueryResults.removeAll(Collections.singleton(null));
            // Extract the cost data corresponding to each cost_code
            ArrayList<EmployeeProfitDetailsVO> employeeProfitDetails = new ArrayList<>();
            for (ProfitDetailsQueryResult profitDetailsResult : profitDetailsQueryResults) {
                EmployeeProfitDetailsVO employeeProfitDetailsVO = new EmployeeProfitDetailsVO();
                String[] split = profitDetailsResult.getCostCode().split("-");
                employeeProfitDetailsVO.setCostCode(profitDetailsResult.getCostCode());
                employeeProfitDetailsVO.setTotalHour(profitDetailsResult.getTotalHour());
                if (Objects.equals(split[split.length-1], "02")){
                    employeeProfitDetailsVO.setCost(profitDetailsResult.getOt_cost());
                }
                else if (Objects.equals(split[split.length-1], "03")){
                    employeeProfitDetailsVO.setCost(profitDetailsResult.getDt_cost());
                }
                else {
                    employeeProfitDetailsVO.setCost(profitDetailsResult.getSt_cost());
                }
                employeeProfitDetails.add(employeeProfitDetailsVO);
            }
            // Calculate the profit of each employee
            for (EmployeeProfitDetailsVO employeeProfitDetail : employeeProfitDetails) {
                BigDecimal bill = ProfitTypeFormulaUtil.getBill(mongoTemplate,
                        employeeProfitDetail.getCostCode(),
                        employeeProfitDetail.getCost(),
                        employeeProfitDetail.getTotalHour());
                employeeProfitDetail.setBilled(bill);
                employeeProfitDetail.setProfit(
                        bill.subtract(employeeProfitDetail.getCost()).floatValue() == 0f ? BigDecimal.ZERO : bill.subtract(employeeProfitDetail.getCost()));
            }
            map.put(employeeId, employeeProfitDetails);
        }
        return map;
    }

    @Override
    public Map<String, ArrayList<CostCodeProfitDetailsVO>> getCostCodeProfitDetails(CostCodeProfitDetailsDTO dto) {
        Map<String, ArrayList<CostCodeProfitDetailsVO>> map = new HashMap<>(16);
        Set<String> costCodes = dto.getCostCodes();
        // Get the corresponding data from costCode
        for (String costCode : costCodes) {
            ArrayList<ProfitDetailsQueryResult> profitDetailsQueryResults = allocationTimeMapper.getCostCodeProfitDetails(dto.getProjectId(),
                    dto.getStartDate(),
                    dto.getEndDate(),
                    costCode);
            // Remove the empty elements in the list
            profitDetailsQueryResults.removeAll(Collections.singleton(null));

            // Extract the cost data corresponding to each cost_code
            ArrayList<CostCodeProfitDetailsVO> costCodeProfitDetails = new ArrayList<>();
            for (ProfitDetailsQueryResult profitDetailsResult : profitDetailsQueryResults) {
                CostCodeProfitDetailsVO costCodeProfitDetailsVO = new CostCodeProfitDetailsVO();
                String[] split = profitDetailsResult.getCostCode().split("-");
                costCodeProfitDetailsVO.setTotalHour(profitDetailsResult.getTotalHour());
                if (Objects.equals(split[split.length-1], "02")){
                    costCodeProfitDetailsVO.setCost(profitDetailsResult.getOt_cost());
                }
                else if (Objects.equals(split[split.length-1], "03")){
                    costCodeProfitDetailsVO.setCost(profitDetailsResult.getDt_cost());
                }
                else {
                    costCodeProfitDetailsVO.setCost(profitDetailsResult.getSt_cost());
                }
                costCodeProfitDetails.add(costCodeProfitDetailsVO);
            }

            // Calculate the profit of each costCode
            for (CostCodeProfitDetailsVO costCodeProfitDetailsVO : costCodeProfitDetails) {
                BigDecimal bill = ProfitTypeFormulaUtil.getBill(
                        mongoTemplate, costCode,
                        costCodeProfitDetailsVO.getCost(),
                        costCodeProfitDetailsVO.getTotalHour());

                costCodeProfitDetailsVO.setBilled(bill);
                costCodeProfitDetailsVO.setProfit(
                        bill.subtract(costCodeProfitDetailsVO.getCost()).floatValue() == 0f ? BigDecimal.ZERO : bill.subtract(costCodeProfitDetailsVO.getCost()));
            }
            map.put(costCode, costCodeProfitDetails);
        }
        return map;
    }

    @Override
    public ArrayList<AllProfitDetailsVO> getAllProfitDetails(AllProfitDetailsDTO dto) {
        // Get the costCode within the specified time and project (de-duplication)
        ArrayList<String> costCodes = allocationTimeMapper.getCostCodeByPayrollDate(
                dto.getProjectId(),
                dto.getStartDate(),
                dto.getEndDate());
        ArrayList<AllProfitDetailsVO> allProfitDetailsVOS = new ArrayList<>();
        for (String costCode : costCodes) {
            AllProfitDetailsVO allProfitDetailsVO = new AllProfitDetailsVO();
            allProfitDetailsVO.setCostCode(costCode);
            // All participating employees of current costCode
            ArrayList<ProfitDetailsQueryResult> profitDetailsQueryResults = allocationTimeMapper.getEmployeeByCostCode(
                    costCode,
                    dto.getProjectId(),
                    dto.getStartDate(),
                    dto.getEndDate());
            // Remove the empty elements in the list
            profitDetailsQueryResults.removeAll(Collections.singleton(null));

            // Extract the cost data corresponding to each cost_code
            ArrayList<EmployeeVO> employeeVOS = new ArrayList<>();
            for (ProfitDetailsQueryResult profitDetailsResult : profitDetailsQueryResults) {
                EmployeeVO employeeVO = new EmployeeVO();
                employeeVO.setTotalHour(profitDetailsResult.getTotalHour());
                employeeVO.setEmployeeID(profitDetailsResult.getEmployeeID());
                employeeVO.setFirstName(profitDetailsResult.getFirstName());
                employeeVO.setLastName(profitDetailsResult.getLastName());
                String[] split = profitDetailsResult.getCostCode().split("-");
                if (Objects.equals(split[split.length-1], "02")){
                    employeeVO.setCost(profitDetailsResult.getOt_cost());
                }
                else if (Objects.equals(split[split.length-1], "03")){
                    employeeVO.setCost(profitDetailsResult.getDt_cost());
                }
                else {
                    employeeVO.setCost(profitDetailsResult.getSt_cost());
                }
                employeeVOS.add(employeeVO);
            }

            // Calculate employee profit
            for (EmployeeVO employeeVO : employeeVOS) {
                BigDecimal bill = ProfitTypeFormulaUtil.getBill(mongoTemplate,
                        costCode,
                        employeeVO.getCost(),
                        employeeVO.getTotalHour());
                employeeVO.setBilled(bill);
                employeeVO.setProfit(bill.subtract(
                        employeeVO.getCost()).floatValue() == 0f ? BigDecimal.ZERO : bill.subtract(employeeVO.getCost()));
            }

            // Calculate costCode profit
            BigDecimal empCostSum = BigDecimal.ZERO;
            BigDecimal empBillSum = BigDecimal.ZERO;
            BigDecimal empProfitSum = BigDecimal.ZERO;
            for (EmployeeVO employeeVO : employeeVOS) {
                empCostSum = empCostSum.add(employeeVO.getCost());
                empBillSum = empBillSum.add(employeeVO.getBilled());
                empProfitSum = empProfitSum.add(employeeVO.getProfit());
            }
            allProfitDetailsVO.setCostCodeCost(empCostSum);
            allProfitDetailsVO.setCostCodeBilled(empBillSum);
            allProfitDetailsVO.setCostCodeProfit(empProfitSum);
            allProfitDetailsVO.setEmployeeVOS(employeeVOS);
            allProfitDetailsVOS.add(allProfitDetailsVO);
        }
        return allProfitDetailsVOS;
    }
}

