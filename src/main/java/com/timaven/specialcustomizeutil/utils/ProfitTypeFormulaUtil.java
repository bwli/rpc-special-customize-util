package com.timaven.specialcustomizeutil.utils;

import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.springframework.data.mongodb.core.MongoTemplate;
import java.math.BigDecimal;
import java.security.InvalidParameterException;
import java.util.Objects;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Projections.fields;
import static com.mongodb.client.model.Projections.include;

public class ProfitTypeFormulaUtil {
    /**
     * Get the bill
     * @param mongoTemplate
     * @param costCode
     * @param cost
     * @param totalHour
     * @return
     */
    public static BigDecimal getBill(MongoTemplate mongoTemplate,
                                     String costCode,
                                     BigDecimal cost,
                                     BigDecimal totalHour) {
        // Get the mongodb collection
        MongoCollection<Document> collection = mongoTemplate.getCollection("profit_details");
        Document projection = collection
                .find(and(eq("cost_code", costCode)))
                .projection(fields(include("profit_type", "profit_amount")))
                .first();
        String profitTypeNumber = projection.getString("profit_type");
        BigDecimal profitAmount = BigDecimal.valueOf(Double.parseDouble(projection.getString("profit_amount")));

        if (Objects.isNull(profitAmount)){
            throw new InvalidParameterException("The profit amount with cost code equal to "+ costCode +" cannot be empty!");
        }
        if (Objects.isNull(profitTypeNumber)){
            throw new InvalidParameterException("The profit type number with cost code equal to "+ costCode +" cannot be empty!");
        }
        if (profitTypeNumber == "3" && (profitAmount.floatValue() >= 1 || profitAmount.floatValue() < 0)){
            throw new InvalidParameterException("The profit amount with cost code equal to "+ costCode +" must be less than 1 and greater than or equal to 0!");
        }
        if (profitTypeNumber == "4" && profitAmount.floatValue() == 0){
            throw new InvalidParameterException("The profit amount whose cost code is equal to "+ costCode +" cannot be equal to 0!");
        }
        return switch (profitTypeNumber) {
            case "1" -> BigDecimal.ZERO;
            case "2" -> cost;
            case "3" -> cost.divide(BigDecimal.ONE.subtract(profitAmount), 6, BigDecimal.ROUND_HALF_UP);
            case "4" -> cost.add(cost.multiply(profitAmount.divide(BigDecimal.valueOf(100))));
            case "5" -> cost.add(profitAmount.multiply(totalHour));
            case "6" -> cost.add(profitAmount);
            case "7" -> profitAmount;
            case "8" -> profitAmount.multiply(totalHour);
            // To be determined
            case "9" -> BigDecimal.ZERO;
            default -> throw new IllegalStateException("Unexpected value: " + profitTypeNumber);
        };
    }
}
