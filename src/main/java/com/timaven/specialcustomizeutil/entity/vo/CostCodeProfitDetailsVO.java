package com.timaven.specialcustomizeutil.entity.vo;

import lombok.Data;
import java.math.BigDecimal;

@Data
public class CostCodeProfitDetailsVO {
    private BigDecimal cost;
    private BigDecimal billed;
    private BigDecimal profit;
    private BigDecimal totalHour;
}
