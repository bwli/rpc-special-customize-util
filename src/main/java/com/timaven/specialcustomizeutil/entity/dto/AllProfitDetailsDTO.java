package com.timaven.specialcustomizeutil.entity.dto;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class AllProfitDetailsDTO {
    @NotNull(message = "projectId not null!")
    private Long projectId;

    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @NotNull(message = "startDate cannot be empty!")
    private LocalDate startDate;

    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @NotNull(message = "endDate cannot be empty!")
    private LocalDate endDate;
}
