package com.timaven.specialcustomizeutil.controller;

import com.timaven.specialcustomizeutil.entity.dto.AllProfitDetailsDTO;
import com.timaven.specialcustomizeutil.entity.dto.CostCodeProfitDetailsDTO;
import com.timaven.specialcustomizeutil.entity.dto.EmployeeProfitDetailsDTO;
import com.timaven.specialcustomizeutil.entity.vo.AllProfitDetailsVO;
import com.timaven.specialcustomizeutil.entity.vo.CostCodeProfitDetailsVO;
import com.timaven.specialcustomizeutil.entity.vo.EmployeeProfitDetailsVO;
import com.timaven.specialcustomizeutil.service.ProfitDetailsService;
import io.swagger.annotations.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Map;

@RestController
@RequestMapping("/v1")
@Api(tags = "ProfitDetailsController", description = "Obtain the corresponding profits of different CostCodes")
public class ProfitDetailsController {
    @Resource
    private ProfitDetailsService profitDetailsService;

    @ApiOperation("employee-profit-details")
    @PostMapping("/employee-profit-details")
    public Map<String, ArrayList<EmployeeProfitDetailsVO>> employeeProfitDetails(@ApiParam @RequestBody @Validated EmployeeProfitDetailsDTO employeeProfitDetailsDTO){
        return profitDetailsService.getEmployeeProfitDetails(employeeProfitDetailsDTO);
    }

    @ApiOperation("cost-code-profit-details")
    @PostMapping("/cost-code-profit-details")
    public Map<String, ArrayList<CostCodeProfitDetailsVO>> costCodeProfitDetails(@ApiParam @RequestBody @Validated CostCodeProfitDetailsDTO costCodeProfitDetailsDTO){
        return profitDetailsService.getCostCodeProfitDetails(costCodeProfitDetailsDTO);
    }

    @ApiOperation("all-profit-details")
    @PostMapping("/all-profit-details")
    public ArrayList<AllProfitDetailsVO> allProfitDetails(@ApiParam @RequestBody @Validated AllProfitDetailsDTO allProfitDetailsDTO){
        return profitDetailsService.getAllProfitDetails(allProfitDetailsDTO);
    }
}
